<?php
if (!defined('AXECTHEME')) {
    exit('What are you doing here??');
}

class childFilters
{
    public static function child_vcard_label_filter($val, $type)
    {
        switch ($type) {
            case 'address':
                $val = '<i class="axeicon fa fa-map-marker-alt"></i>';
                break;
            case 'email':
                $val = '<i class="axeicon far fa-envelope-open"></i>';
                break;
            case 'phone':
                $val = '<i class="axeicon fa fa-phone fa-rotate-90"></i>';
                break;
            case 'fax':
                $val = '<i class="axeicon fa fa-fax"></i>';
                break;
            case 'officehours':
                $val = '<i class="axeicon fa fa-clock"></i>';
                break;
        }

        return $val;
    }
    public static function child_vcard_value_filter($val, $type)
    {
        switch ($type) {
            case 'phone':
                $val = '<a href="tel:' . $val . '">' . $val . '</a>';
                break;
            case 'email':
                $val = '<a href="mailto:' . $val . '">' . $val . '</a>';
                break;
        }

        return $val;
    }
    public static function child_logoclass_filter($cls)
    {
        return str_replace('w3', 'w4', $cls);
    }
}
