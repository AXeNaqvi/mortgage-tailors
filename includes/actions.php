<?php
if (!defined('AXECTHEME')) {
    exit('What are you doing here??');
}

class childActions
{
	public static function axechild_prefetch(){
		echo '<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>';	
	}
    public static function axechild_loadcss()
    {
        wp_dequeue_style('themeaxe-style-css');
        wp_enqueue_style('themeaxe-maincss', THEMEAXEPATH . '/style.css', false, THEMEAXEVER, false);
        wp_enqueue_style(AXECTHEME . '-style-css', CURRENTTHEMEAXEPATH . '/style.css', false, CTHEMEVER, false);
    }
    public static function axechild_loadjs()
    {
        wp_dequeue_script('jquery-effects-core');
        wp_enqueue_script('jquery-effects-core');
        wp_enqueue_script('themeaxe-child-js', CURRENTTHEMEAXEPATH . '/js/script.js', array('jquery'), CTHEMEVER, true);
    }
    private static function axe_mobilemenu()
    {
        echo '<div id="axemobilecaller"><div class="axeflex"><div class="w w3 axemobilecaller"><a href="#"><span class="fa fa-bars"></span></a></div><div class="w w9 ctabtn"><a href="/book-appointment">Book Appointment</a></div></div></div>';
    }
    public static function axechild_logomenu_start()
    {
        ?>
<div class="axeflex">
    <div class="w w4 header_call"><?php themeaxe_sidebar('header-contact');?></div>
    <?php
}
    public static function axechild_logomenu_ends()
    {
        ?>
    <div class="w w4 header_social"><?php themeaxe_sidebar('header-social');?></div>
</div>
<?php
}

    public static function axechild_main_title()
    {
        $title = get_the_title();
        if ($title) {
            ?>
<div class="axechild_titleheading">
    <div class="wrapwidth">
        <h1 class="<?php echo apply_filters('axe_main_title_class_filter', 'titleheading'); ?>">
            <?php
do_action('before_axe_page_title');
            echo apply_filters('axe_main_title_filter', $title);
            do_action('after_axe_page_title');
            ?>
        </h1>
    </div>
</div>
<?php
}
    }

    public static function axechild_mainmenu()
    {
        $class = apply_filters('axe_mainmenuclass_filter', 'axemainmenu');
        ?>
<div class="primarybg allwhite childmainmenu">
    <div class="wrapwidth">
		<?php self::axe_mobilemenu(); ?>
        <div id="mainmenu" class="<?php echo $class; ?>">
            <?php do_action('axe_above_main_nav');
        $args = apply_filters('axe_menu_theme_location', array('theme_location' => 'primary'));
        wp_nav_menu($args);
        do_action('axe_after_main_nav');
        ?>
        </div>
    </div>
</div>
<?php
}
}