;
(function($){
	if (window.location.hash) {
		window.scrollTo(0, 0);
	}
	var axetarget = $(window.location.hash),
	mobilelimit = 960,
	megatitle = 'Edmonton Mortgage Tailors';
	$(document).ready(function(){
		mobileNavHook();
		document.addEventListener( 'wpcf7invalid', function( event ) {
			axeHideContactFormErrors();
		}, false );
		document.addEventListener( 'wpcf7spam', function( event ) {
			axeHideContactFormErrors();
		}, false );
	});

	$(window).resize(function () {
		removeNavControlStyle($('#axemobilecaller .axemobilecaller > a'));
		/*wrapperBottomPadding();*/
	});

	function axeHideContactFormErrors(){
		$('span.wpcf7-not-valid-tip, .wpcf7-response-output').on('click',function(e){
			e.preventDefault();
			e.stopPropagation();
			var t = $(this),
			sib = t.siblings('.wpcf7-form-control');
			t.slideUp();
			sib.focus();
		});
		$('.wpcf7-form-control').on('focus',function(){
			var t = $(this),
			sib = t.siblings('span.wpcf7-not-valid-tip');
			sib.slideUp();
		});
	}
	function mobileNavHook(){
		$('#axemobilecaller .axemobilecaller > a').off('click').on('click',function(e){
			e.stopPropagation();
			e.preventDefault();
			var t = $(this),
			p = t.parents('.childmainmenu');
			$('.axemainmenu',p).slideToggle();
			$('span.fa',t).toggleClass('fa-bars').toggleClass('fa-times');
			removeNavControlStyle(t);
		});
	}
	function removeNavControlStyle(t){
		if($(window).width() > mobilelimit){
			p = t.parents('.childmainmenu');
			$('.axemainmenu',p).removeAttr('style');
			$('span.fa',t).removeClass('fa-times').addClass('fa-bars');
		}
	}
})(jQuery);